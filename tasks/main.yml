---
- name: Add distribution-specific variables
  include_vars: "Debian.yml"

- name: Gather instance facts
  setup:

- include_tasks: install.yml

- name: Enable wireguard kernel module
  modprobe:
    name: wireguard
    state: present
  register: wireguard_module_enabled
  until: wireguard_module_enabled is succeeded
  retries: 10
  delay: 10
  failed_when: wireguard_module_enabled is failure

- name: Get wg subcommands
  command: "wg --help"
  register: wg_subcommands
  changed_when: false

- name: Set default value for wg_syncconf variable (assume wg syncconf subcommand not available)
  set_fact:
    wg_syncconf: false

- name: Check if wg syncconf subcommand is available
  set_fact:
    wg_syncconf: true
  when: wg_subcommands.stdout | regex_search('syncconf:')

- name: Ensure wireguard directory exists
  file:
    path: "/etc/wireguard"
    state: directory
    owner: root
    group: root
    mode: 0640

- name: Register if private key already exists
  stat:
    path: "/etc/wireguard/wg.priv"
  register: wg_priv_file_stat

- block:
    - name: Generate a wireguard private key
      shell: "umask 077; wg genkey > /etc/wireguard/wg.priv"
      args:
        creates: "/etc/wireguard/wg.priv"
  when: not wg_priv_file_stat.stat.exists

- block:
    - name: Slurp the wireguard private key file
      slurp:
        path: "/etc/wireguard/wg.priv"
      register: wg_private_key_file
      changed_when: false

    - name: Set private key fact
      set_fact:
        wireguard_private_key: "{{ wg_private_key_file['content'] | b64decode | trim }}"

- name: Compute the wireguard public key
  shell: "wg pubkey < /etc/wireguard/wg.priv"
  register: "wireguard_public_key_cmd"
  changed_when: false

- name: Register the wireguard public key as a fact
  set_fact:
    wireguard_public_key: "{{ wireguard_public_key_cmd.stdout | trim }}"

- name: Write the wireguard config
  template:
    src: "wg0.conf.j2"
    dest: "/etc/wireguard/{{ wireguard_interface }}.conf"
    owner: root
    group: root
    mode: 0640
  notify:
    - reconfigure wireguard

- name: Write the wg interface config
  file:
    path: "/etc/network/interfaces.d/60-wireguard.cfg"
    state: absent

- name: Update /etc/hosts
  lineinfile:
    dest: /etc/hosts
    regexp: "^{{ hostvars[host]['wireguard_ip'] | regex_replace('\\.','\\\\.') }} "
    line: "{{ hostvars[host]['wireguard_ip'] }} {{ host }}"
  with_items: "{{ groups[wireguard_group] }}"
  loop_control:
    loop_var: host
  when: host != inventory_hostname and hostvars[host]['wireguard_ip'] is defined

- name: Enable IPv4 forwarding
  sysctl:
    name: net.ipv4.ip_forward
    value: '1'
    reload: true

- name: Enable and start wireguard service
  service:
    name: "wg-quick@{{ wireguard_interface }}"
    state: started
    enabled: true

- name: Check if reload-module-on-update is set
  stat:
    path: "/etc/wireguard/.reload-module-on-update"
  register: reload_module_on_update

- name: Set wireguard reload-module-on-update
  file:
    dest: "/etc/wireguard/.reload-module-on-update"
    state: touch
  when: not reload_module_on_update.stat.exists

- name: Start and enable wireguard service
  service:
    name: "wg-quick@{{ wireguard_interface }}"
    state: started
    enabled: true
