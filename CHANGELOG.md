# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/guardianproject-ops/ansible-wireguard/compare/1.0.0...1.0.1) (2020-05-12)


### Bug Fixes

* Remove errant typo in config ([50c31c4](https://gitlab.com/guardianproject-ops/ansible-wireguard/commit/50c31c4222cea36a2039cd1555485b76854268e6))

## 1.0.0 (2020-05-12)
