import os
import yaml
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.fixture(scope="module")
def AnsibleVars(host):
    stream = host.file("/tmp/ansible-vars.yml").content
    return yaml.safe_load(stream)


@pytest.fixture(scope="module")
def wireguard_ip(host, AnsibleVars):
    return AnsibleVars["wireguard_ip"]


@pytest.fixture(scope="module")
def peer_ip(host, AnsibleVars):
    my_ip = AnsibleVars["wireguard_ip"]
    if my_ip == "10.1.1.1":
        other_ip = "10.1.1.2"
    else:
        other_ip = "10.1.1.1"
    return other_ip


def test_wg0_address(host, wireguard_ip):
    with host.sudo():
        wg0 = host.file("/etc/wireguard/wg0.conf")
        assert wg0.exists
        assert wg0.contains(f"Address = {wireguard_ip}")


@pytest.mark.parametrize("name", ["wg-quick@wg0"])
def test_services(host, name):
    service = host.service(name)
    assert service.is_running
    assert service.is_enabled


def test_ping_peer(host, peer_ip):
    res = host.run(f"ping -c 1 {peer_ip}")
    assert res.rc == 0
