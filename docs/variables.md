## Role Variables

* `wireguard_subnet`: `` - the subnet of your wireguard network.


  ```yaml
  wireguard_subnet: '10.1.1.0/24'
  ```

* `wireguard_ip`: `` - the ip this node is assigned. every host must have a different wireguard_ip


  ```yaml
  wireguard_ip: '10.1.1.1'
  ```

* `wireguard_endpoint`: `"{{ ansible_default_ipv4 }}"` - the ip address that othere peers can use to connect to the host



* `wireguard_extra_peers`: `[]` - list of extra peers that should be added to the host. a list of objects with the keys: `public_key`, `ip`, and `name`


  ```yaml
  wireguard_extra_peers:
    - public_key: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=
      ip: 10.2.1.250
      name: AliceAdmin
    - public_key: BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB=
      ip: 10.2.1.251
      name: TonyTester
  ```

* `wireguard_group`: `` - all hosts in this ansible group are added to the wireguard network mesh



* `wireguard_listen_port`: `51820` - the udp port wireguard will listen to connections on



* `wireguard_interface`: `wg0` - the name of the wg managed interface



* `wireguard_install_kernel_module`: `true` - whether or not to install the wireguard dynamic kernel module.



* `wireguard_packages`: `['wireguard']` - the package(s) that install wireguard


